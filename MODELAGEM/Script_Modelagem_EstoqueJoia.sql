-- Database diff generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.2
-- PostgreSQL version: 9.5

-- [ Diff summary ]
-- Dropped objects: 0
-- Created objects: 0
-- Changed objects: 16
-- Truncated tables: 0

SET search_path=public,pg_catalog,seguranca,comercial,operacional;
-- ddl-end --


-- [ Changed objects ] --
COMMENT ON CONSTRAINT fk_produto_subcategoria ON comercial.produto  IS 'Chave estrangeira por subcategoria';
-- ddl-end --
COMMENT ON CONSTRAINT fk_precoproduto_produto ON comercial.precoproduto  IS 'Chave estrangeira por produto';
-- ddl-end --
COMMENT ON CONSTRAINT fk_usuario_empresa ON seguranca.usuario  IS 'Chave estrangeira por empresa';
-- ddl-end --
COMMENT ON CONSTRAINT fk_entradaproduto_entradaestoque ON operacional.entradaproduto  IS 'Chave estrangeira por entrada estoque';
-- ddl-end --
COMMENT ON CONSTRAINT fk_entradaproduto_produto ON operacional.entradaproduto  IS 'Chave estrangeira por produto';
-- ddl-end --
COMMENT ON CONSTRAINT fk_subcategoria_categoria ON comercial.subcategoria  IS 'chave estrangeira por categoria';
-- ddl-end --
COMMENT ON CONSTRAINT fk_produto_empresa ON comercial.produto  IS 'Chave estrangeira por empresa';
-- ddl-end --
COMMENT ON CONSTRAINT fk_entradaestoque_empresa ON operacional.entradaestoque  IS 'Chave estrangeira por empresa';
-- ddl-end --
ALTER TABLE seguranca.empresa ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE seguranca.empresa ALTER COLUMN id SET DEFAULT nextval('seguranca.empresa_id_seq'::regclass);
-- ddl-end --
ALTER TABLE comercial.produto ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE comercial.produto ALTER COLUMN id SET DEFAULT nextval('comercial.produto_id_seq'::regclass);
-- ddl-end --
ALTER TABLE comercial.precoproduto ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE comercial.precoproduto ALTER COLUMN id SET DEFAULT nextval('comercial.precoproduto_id_seq'::regclass);
-- ddl-end --
ALTER TABLE seguranca.usuario ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE seguranca.usuario ALTER COLUMN id SET DEFAULT nextval('seguranca.usuario_id_seq'::regclass);
-- ddl-end --
ALTER TABLE comercial.categoria ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE comercial.categoria ALTER COLUMN id SET DEFAULT nextval('comercial.categoria_id_seq'::regclass);
-- ddl-end --
ALTER TABLE comercial.subcategoria ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE comercial.subcategoria ALTER COLUMN id SET DEFAULT nextval('comercial.subcategoria_id_seq'::regclass);
-- ddl-end --
ALTER TABLE operacional.entradaestoque ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE operacional.entradaestoque ALTER COLUMN id SET DEFAULT nextval('operacional.entradaestoque_id_seq'::regclass);
-- ddl-end --
ALTER TABLE operacional.entradaproduto ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE operacional.entradaproduto ALTER COLUMN id SET DEFAULT nextval('operacional.entradaproduto_id_seq'::regclass);
-- ddl-end --
