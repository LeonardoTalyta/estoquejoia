
package Bean;

import Model.LoginEntity;
import java.io.Serializable;

public class loginController implements Serializable {
   private LoginEntity login;
   
   public loginController (){
       login = new LoginEntity();
   }

    public LoginEntity getLogin() {
        return login;
    }

    public void setLogin(LoginEntity login) {
        this.login = login;
    }
   
}
