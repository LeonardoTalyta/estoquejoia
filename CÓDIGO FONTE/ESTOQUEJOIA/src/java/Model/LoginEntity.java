
package Model;

public class LoginEntity {
    public static final long serialVersionUID = 1l;
    
    private String login;
    private String senha;

    public LoginEntity(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    public LoginEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
}
