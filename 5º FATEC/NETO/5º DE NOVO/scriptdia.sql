create database timesdia;
use listatimes;

create table estados(
	id int not null unique auto_increment primary key,
    nome varchar(255) not null unique,
	sigla char(2) not null
) ENGINE = Innodb;

create table divisoes(
	id int not null auto_increment unique primary key,
    nome varchar(255) not null unique
)ENGINE Innodb;

create table times (
	id int not null unique auto_increment primary key,
    nome varchar(255) not null unique,
    id_estado int not null,
    id_divisao int not null,
    constraint fkidestado foreign key(id_estado) references estados(id) on delete cascade,
    constraint fkiddivisao foreign key(id_divisao) references divisoes(id) on delete cascade
) ENGINE = Innodb;

insert into estados(nome,sigla) values
	('Acre','AC'),
    ('Alagoas','AL'),
    ('Amapá','AP'),
    ('Amazonas','AM'),
    ('Bahia','BA'),
    ('Ceará','CE'),
    ('Distrito Federal','DF'),
    ('Espírito Santo','ES'),
    ('Goiás','GO'),
    ('Maranhão','MA'),
    ('Mato Grosso','MT'),
    ('Mato Grosso do Sul','MS'),
    ('Minas Gerais','MG'),
    ('Pará','PA'),
    ('Paraíba','PB'),
    ('Paraná','PR'),
    ('Pernambuco','PB'),
    ('Piauí','PI'),
    ('Rio de Janeiro','RJ'),
    ('Rio Grande do Norte','RN'),
    ('Rio Grande do Sul','RS'),
    ('Rondônia','RO'),
    ('Roraima','RR'),
    ('Santa Catarina','SC'),
    ('São Paulo','SP'),
    ('Sergipe','SE'),
    ('Tocantins','TO');

insert into divisoes(nome) values('Primeira Divisão'), ('Segunda Divisão');

    