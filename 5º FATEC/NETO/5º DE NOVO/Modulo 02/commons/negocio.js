var porcentagem = 1.50;
var quantidade = 100;

exports.custoVista = function (valorProduto){
	return valorProduto * porcentagem;
}

exports.custoPrazo = function (valorProduto){
	return (valorProduto * porcentagem)* 2;
}

exports.precoCompra = function(valorProduto){
	return valorProduto * quantidade;
}

exports.pesoProduto = function(peso){
	return peso * quantidade;
}
