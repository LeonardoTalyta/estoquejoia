var pessoa = require('./commons/pessoa');
var somar = require('./commons/soma');
var multiplicar = require('./commons/multiplicacao');
var dividir = require('./commons/divisao');
var subtrair = require('./commons/subtracao');
var potencia = require('./commons/potencia');
var imposto = require('./commons/calculo');


pedro = pessoa();
console.log(JSON.stringify(pedro));
console.log("Total da soma: ", somar(5,2));
console.log("Total da multiplicação: ", multiplicar(5,2));
console.log("Total da divisão: ", dividir(5,2));
console.log("Total da subtração: ", subtrair(5,2));
console.log("Total da potenciação: ", potencia(5,2));

console.log('Valor do Produto com imposto: ' + imposto.adicionar());
console.log('Valor do imposto: ' + imposto.valor(3));
console.log('Taxa do imposto: ' + imposto.taxa);
