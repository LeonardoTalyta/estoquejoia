CREATE DATABASE RESTRICAO

USE RESTRICAO

CREATE TABLE Professores(
	CodProf int constraint Pk_codProf primary key identity (1,1),
	nome varchar(80)not null,
	RG numeric(12) UNIQUE,
	sexo char(1) check(sexo in ('M', 'F')),
	idade int check (idade between 21 and 80),
	cidade varchar(50) CONSTRAINT DF_Professores_cidade DEFAULT ('FRANCA'),
	titulacao varchar(15) check(titulacao in ('graduacao','especialista','mestre','doutor')),
	categoria varchar(15) check (categoria in('auxiliar','assistente','adjunto','titular')),
	salario money check (salario >= 500) 
)


select *from Professores

alter table Professores
add
 constraint ch_titulacao_salario check(
	(titulacao = 'graduacao' and salario < 1000)
	or
	(titulacao <> 'graduacao')
 )
update Professores set salario = 750



insert into Professores(RG,sexo,idade,titulacao,categoria,salario,nome)
values
	(123456,'M',25,'graduado','auxiliar',1515.99,'JOOAAO')
	
	
insert into Professores(RG,sexo,idade,titulacao,categoria,salario,nome)
values
	(101010,'M',25,'graduacao','titular',999,'PEDRO')



select *from Professores