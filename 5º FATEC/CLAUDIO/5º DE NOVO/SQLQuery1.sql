use Ads5N

select * from cd
	inner join categoria as cat
		on cat.codCat = cd.codCat
		and cat.ativo = 'S'
		
update cd set preco = preco * 1.1
	where ano between 1950 and 2002
	
select * from cd 
	where preco >= (select AVG(preco) from cd)
	
delete cd 
	where codArt in (select codArt from artista where pais = 'BRASIL')
	and codCat in (select codCat from categoria where ativo <> 'S')