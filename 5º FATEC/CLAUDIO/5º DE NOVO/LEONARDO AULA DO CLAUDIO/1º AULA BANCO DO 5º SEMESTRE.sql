CREATE DATABASE Ads5N
GO
use Ads5N


CREATE TABLE Categoria (
	CodCat int PRIMARY KEY identity (1,1),
	descricao varchar(30),
	ativo char(1)not null
)

CREATE TABLE Artista (
	CodArt int PRIMARY KEY identity (100,1),
	Nome varchar(50),
	DtNascimento datetime,
	pais varchar(20)
)


CREATE TABLE CD (
	CodCD int PRIMARY KEY identity (1,1),
	titulo varchar(20),
	ano int,
	codCat int foreign key references Categoria (codCat),
	codArt int foreign key references Artista (codArt)
	)
	
	
	
	
	INSERT INTO Categoria VALUES('ROCK','S'),
								 ('POP','S'),
								 ('FUNK','N'),
								 ('SERTANEJO','N')
								 
								 
	
	
	INSERT INTO Artista VALUES('JOSE DA SILVA','1950/01/15','BRASIL','RAUL SEIXAS'),
								 ('SEBASTI�O DA SILVA','1980/02/12','BRASIL','TI�O CARREIRO'),
								 ('RENATO FERNANDO','1982/04/20','EUA','LEGIAO URBANO'),
								 ('JOSE DA SILVA','1981/05/18','ARGENTINA','TIT�S')
								 
								 
SET DATEFORMAT DMY -- D = DAY   M = MONTH   Y= YEAR

								 

INSERT INTO CD (ano, codArt, titulo)VALUES
								 (2011,101,'NOVO AMANHA'),
								 (2010,102,'VOLUME 2'),
								 (2015,101,'DOSE DUPLA'),
								 (1995,101,'PERFIL')
								 
								 
								 
								 
ALTER TABLE CD ADD VALOR DECIMAL (10,2) 
								 SELECT *FROM Artista
								 
								 
								 
								 
								 
								 
UPDATE CD SET valor = 1.99
WHERE codArt = 101


UPDATE CD SET VALOR =7.50
WHERE codArt = 102

UPDATE CD SET VALOR = 10.40
WHERE codArt = 103





SELECT * FROM CD
SELECT * FROM Categoria


INSERT INTO Artista VALUES('RAMISTHEN', '20/10/1985','ALEMANHA','RAMISTHEN')


--7
INSERT INTO CD (ano, codArt, titulo)
VALUES (1999, 105, 'BEFORE FORGET'),
		(2002, 105, 'I FELL'),
		(2008, 105, 'BALD')

UPDATE CD SET VALOR = 100.99
WHERE codArt = 105


UPDATE CD SET codCat = 1
WHERE codArt = 105

SELECT *FROM CD 
	WHERE codArt IN (SELECT codArt FROM Artista WHERE pais = 'BRASIL')
	
	
	
	SELECT * FROM CD as c INNER JOIN Artista as AR
	ON c.codArt = AR.CodArt
	WHERE pais = 'BRASIL'
	
	
	select *from CD as c inner join Categoria as cat
	on c.codCat = cat.CodCat 
	WHERE cat.ativo = 'S'
	
	
	--9
	UPDATE CD SET VALOR = VALOR * 1.1
	WHERE ano BETWEEN 1950 AND 2002
	
	--10 LISTE OS CDS COM PRECO ACIMA DA M�DIA
	SELECT *FROM CD       -- AVG FAZ A M�DIA
	WHERE VALOR >= (SELECT AVG(VALOR) FROM CD)
	
	
	
	--11
	DELETE CD WHERE codArt in (SELECT codArt from Artista where pais = 'BRASIL') AND
							 codCat in  (SELECT codCat from Categoria where ativo <> 'S')
							 
	
	select COUNT(*) AS TOTAL
	from CD inner join Artista as art 
		on cd.codArt = art.CodArt
		where pais = 'ALEMANHA'
	